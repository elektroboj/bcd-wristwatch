BCD Wristwatch
==============

![BCD Wristwatch Photo](doc/Wristwatch.jpg)


This is a simple project made for eStudents' workshops.



Making Your Own BCD Wristwatch
------------------------------

You need to order your own PCB (it is designed using Eagle) and components (as suggested by BOM).
SVG displaying component placement can be printed to assist You during the assembly.

MCU can be programmed before (recommended) or after soldering with a hex file in the download section.
