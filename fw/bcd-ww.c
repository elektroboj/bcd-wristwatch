#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <avr/io.h>

#include "refresher.h"
#include "display.h"
#include "clock.h"
#include "keys.h"


#define SHOW_TIME_DURATION_MS	5000
#define WAIT_DEBOUNCE_MS		100
#define DISPLAY_TEST_TICK_MS	100
#define ADJUST_TIMEOUT_MS		8000
#define ADJUST_AUTO_INC_MS		200


typedef enum {
	STATE_IDLE = 0,
	STATE_READY,
	STATE_WAIT_DEBOUNCE,
	STATE_DISPLAY_TEST,
	STATE_SHOW_TIME,
	STATE_ADJUST_TIME,
	STATE_ADJUST_HOURS,
	STATE_ADJUST_MINUTES,
} ww_state_t;


static ww_state_t ww_state = STATE_IDLE;
static ticks_t ts_timeout;
static uint16_t display_test;
static clock_hm_t adj_hm;
static bool adj_auto_inc;


static void keys_handler_default(uint8_t id, keys_state_t state);
static void keys_handler_adjust(uint8_t id, keys_state_t state);
static void display_test_next(void);
static void adjust_next(void);
static void adjust_inc(void);


static void start_timeout(void) {
	ts_timeout = refresher_get_ticks();
}

static bool is_timeout(uint16_t to) {
	return (ticks_t)(refresher_get_ticks() - ts_timeout) > MS2TICKS(to);
}

static void enter_ready(void) {
	if (ww_state == STATE_IDLE) {
		display_init();
		refresher_start();
	} else {
		display_show_bin(0x00, 0x00);
	}
	ww_state = STATE_READY;
	keys_set_handler(keys_handler_default);
}

static void enter_wait_debounce(void) {
	ww_state = STATE_WAIT_DEBOUNCE;
	start_timeout();
}

static void enter_idle(void) {
	ww_state = STATE_IDLE;
	refresher_stop();
	keys_enable_wakeup();
}

static void enter_show_time(void) {
	ww_state = STATE_SHOW_TIME;
	keys_set_handler(keys_handler_default);
	start_timeout();
}

static void enter_adjust_time(void) {
	ww_state = STATE_ADJUST_TIME;
	keys_set_handler(keys_handler_adjust);
	adj_hm = clock_read();
	adj_auto_inc = false;
	display_show(adj_hm.pm, adj_hm.hours, 0);
	start_timeout();
}

static void enter_adjust_hours(void) {
	ww_state = STATE_ADJUST_HOURS;
}

static void enter_adjust_minutes(void) {
	ww_state = STATE_ADJUST_MINUTES;
	display_show(false, 0, adj_hm.minutes);
}

static void enter_display_test(void) {
	ww_state = STATE_DISPLAY_TEST;
	display_test = 1;
	display_test_next();
	keys_set_handler(keys_dummy_handler);
}

static void keys_handler_default(uint8_t id, keys_state_t state) {
	if (id == 0) {
		if (state == KEY_DOWN) {
			enter_show_time();
		} else if (state == KEY_HOLD) {
			enter_display_test();
		}
	} else if (id == 1) {
		if (state == KEY_HOLD) {
			enter_adjust_time();
		}
	}
}

static void keys_handler_adjust(uint8_t id, keys_state_t state) {
	start_timeout();

	if (id == 0) {
		adj_auto_inc = false;
		if (state == KEY_DOWN) {
			adjust_inc();
		} else if (state == KEY_HOLD) {
			adj_auto_inc = true;
		}
	} else if (id == 1) {
		if (state == KEY_UP) {
			// adjust next thing
			adjust_next();
		} else if (state == KEY_HOLD) {
			// abort adjusting
			enter_ready();
		}
	}
}

static void display_test_next(void) {
	uint8_t display_test_hi, display_test_lo;
	start_timeout();
	display_test_lo = display_test & 0xFF;
	display_test_hi = display_test >> 8;
	display_test = display_test << 1;

	if (display_test_lo) {
		display_show_bin(display_test_lo, 0x00);
	} else if (display_test_hi) {
		display_show_bin(0x00, display_test_hi);
	} else {
		enter_ready();
	}
}

static void adjust_next(void) {
	if (ww_state == STATE_ADJUST_TIME) {
		// start adjusting hours
		enter_adjust_hours();
	} else if (ww_state == STATE_ADJUST_HOURS) {
		// switch to adjusting minutes
		enter_adjust_minutes();
	} else {
		// store the new time
		clock_set(adj_hm);
		// display the new time
		enter_show_time();
	}
}

static void adjust_inc(void) {
	if (ww_state == STATE_ADJUST_HOURS) {
		++adj_hm.hours;
		if (adj_hm.hours > 12) {
			adj_hm.hours = 1;
			adj_hm.pm = !adj_hm.pm;
		}
		display_show(adj_hm.pm, adj_hm.hours, 0);
	} else {
		++adj_hm.minutes;
		if (adj_hm.minutes >= 60) {
			adj_hm.minutes = 0;
		}
		display_show(false, 0, adj_hm.minutes);
	}
}

static void state_task(void) {
	switch (ww_state) {
		case STATE_SHOW_TIME:
			// go to ready after a timeout
			// otherwise update display with the current time
			if (is_timeout(SHOW_TIME_DURATION_MS)) {
				enter_ready();
			} else {
				clock_hm_t hm = clock_read();
				display_show(hm.pm, hm.hours, hm.minutes);
			}
			break;

		case STATE_ADJUST_TIME:
		case STATE_ADJUST_HOURS:
		case STATE_ADJUST_MINUTES:
			if (!keys_read() && is_timeout(ADJUST_TIMEOUT_MS)) {
				// abort time adjusting on timeout
				enter_ready();
			} else if (adj_auto_inc && is_timeout(ADJUST_AUTO_INC_MS)) {
				// auto increment time
				adjust_inc();
				start_timeout();
			}
			break;

		case STATE_READY:
			// go to idle if no keys are pressed
			if (!keys_read()) {
				enter_idle();
			}
			break;

		case STATE_WAIT_DEBOUNCE:
			// return to idle (via ready) on timeout
			if (is_timeout(WAIT_DEBOUNCE_MS)) {
				enter_ready();
			}
			break;

		case STATE_DISPLAY_TEST:
			if (is_timeout(DISPLAY_TEST_TICK_MS)) {
				display_test_next();
			}
			break;

		default:
			break;
	}
}

static void low_power_init(void) {
	// disable analog comparator
	ACSR |= _BV(ACD);

	// disable TIM0, USI and ADC
	// note that TIM0 will be powered up/down along with refresher
	PRR = _BV(PRTIM0) | _BV(PRUSI) | _BV(PRADC);

	// init sleep mode (idle is the only option as we need to keep track of time)
	set_sleep_mode(SLEEP_MODE_IDLE);
}

static void sleep_task(void) {
	sleep_enable();
	if (ww_state == STATE_IDLE) {
		cli();
		if (keys_wakeup_requested()) {
			keys_disable_wakeup();
			sei();
			enter_ready();
			enter_wait_debounce();
		}
	}
	sei();
	sleep_cpu();
	sleep_disable();
}

int main() {
	low_power_init();
	clock_init();
	keys_init();
	enter_ready();
	enter_show_time();
	sei();

	for (;;) {
		keys_task();
		state_task();
		sleep_task();
	}
}
