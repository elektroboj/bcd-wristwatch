#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#include "clock.h"


static uint8_t seconds;
static clock_hm_t hm = {
	.hours = 12,
	.pm = true,
};


ISR(TIM1_OVF_vect, ISR_NOBLOCK) {
	seconds += 2;

	if (seconds >= 60) {
		seconds -= 60;
		++hm.minutes;

		if (hm.minutes >= 60) {
			hm.minutes = 0;
			++hm.hours;

			if (hm.hours > 12) {
				hm.hours = 1;
				hm.pm = !hm.pm;
			}
		}
	}
}

static inline void clock_enable_int(void) {
	TIMSK1 |=  _BV(TOIE1);
}

static inline void clock_disable_int(void) {
	TIMSK1 &= ~_BV(TOIE1);
}

void clock_init(void) {
	TCCR1A = 0x00;
	TCCR1B = _BV(CS10);
	clock_enable_int();
}

clock_hm_t clock_read(void) {
	clock_hm_t safe_hm;
	clock_disable_int();
	safe_hm = hm;
	clock_enable_int();
	return safe_hm;
}

void clock_set(clock_hm_t new_hm) {
	clock_disable_int();
	hm = new_hm;
	seconds = 0;
	TCNT1 = 0;
	TIFR1 |= _BV(TOV1);
	clock_enable_int();
}
