#ifndef _CLOCK_H_
#define _CLOCK_H_

#include <stdint.h>
#include <stdbool.h>


typedef struct {
	uint8_t hours;
	uint8_t minutes;
	bool pm;
} clock_hm_t;


void clock_init(void);
clock_hm_t clock_read(void);
void clock_set(clock_hm_t hm);

#endif
