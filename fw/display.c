#include <stdbool.h>
#include <stdint.h>

#include <avr/io.h>

#include "refresher.h"
#include "display.h"


uint8_t display_leds0, display_leds1;


void display_init(void) {
	display_leds0 = 0x00;
	display_leds1 = 0x00;
}

static uint8_t to_bcd(uint8_t b) {
	uint8_t q, r;
	// use simple, but fast division (at least much faster than / and %)
	for (q = 0, r = b; r >= 10; ++q, r -= 10);
	return (q << 4) | r;
}

void display_show(bool ampm, uint8_t hours, uint8_t minutes) {
	uint8_t row0, row1;

	row0 = (ampm ? 1 << 5 : 0) | (to_bcd(hours) & 0x1F);
	row1 = to_bcd(minutes);

	display_show_bin(row0, row1);
}

void display_show_bin(uint8_t row0, uint8_t row1) {
	uint8_t leds0, leds1;

	row0 &= 0x3F;
	row1 &= 0x7F;

	leds0 = (row0 << 1) | (row1 & 1);
	leds1 = ~row1 | _BV(PA7);

	display_show_raw(leds0, leds1);
}

void display_show_raw(uint8_t leds0, uint8_t leds1) {
	uint8_t old_timsk0;

	old_timsk0 = refresher_pause();
	display_leds0 = leds0;
	display_leds1 = leds1;
	refresher_resume(old_timsk0);
}
