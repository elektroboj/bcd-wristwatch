#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include <stdbool.h>
#include <stdint.h>


void display_init(void);
void display_show(bool ampm, uint8_t hours, uint8_t minutes);
void display_show_bin(uint8_t row0, uint8_t row1);
void display_show_raw(uint8_t leds0, uint8_t leds1);
void display_refresh(void);

#endif
