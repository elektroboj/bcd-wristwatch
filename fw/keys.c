#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#include "refresher.h"
#include "keys.h"


#define KEYS_HOLD_TIME_TICKS	MS2TICKS(4000)


volatile bool _keys_wakeup_requested = false;
volatile uint8_t _keys_history[2];
static uint8_t keys_prev;
static ticks_t keys_ts[2];
static keys_handler_t keys_handler = keys_dummy_handler;


void keys_init(void) {
	uint8_t i;

	// enable reading S1 and S2
	DDRA = 0x00;
	PORTA = 0xFF;	// eliminate unconnected inputs with pull-ups
	DDRB &= ~_BV(PB2);
	PORTB |= _BV(PB2);

	// keys are up initially; let debouncer do its thing if not
	keys_prev = 0x00;
	for (i = 0; i < 2; ++i) {
		_keys_history[i] = 0xFF;
	}

	// no wakeup request
	_keys_wakeup_requested = false;
}

void keys_set_handler(keys_handler_t kh) {
	keys_handler = kh;
}

void keys_dummy_handler(uint8_t id, keys_state_t state) {
}

void keys_task(void) {
	uint8_t i, key_mask;

	for (i = 0, key_mask = 1; i < 2; ++i, key_mask <<= 1) {
		uint8_t key_was_pressed = keys_prev & key_mask;
		uint8_t cur_keys_history = _keys_history[i];

		if ((cur_keys_history == 0xFF &&  key_was_pressed) ||
		    (cur_keys_history == 0x00 && !key_was_pressed)) {
			keys_prev ^= key_mask;
			keys_handler(i, !key_was_pressed);
			keys_ts[i] = refresher_get_ticks();
		} else if (key_was_pressed &&
		           ((uint8_t)(refresher_get_ticks() - keys_ts[i]) > KEYS_HOLD_TIME_TICKS)) {
			// NOTE: this will repeat with every HOLD_TIME, but it doesn't really matter
			keys_handler(i, KEY_HOLD);
			keys_ts[i] = refresher_get_ticks();
		}
	}
}

uint8_t keys_read(void) {
	return keys_prev;
}

ISR(PCINT0_vect, ISR_NAKED) {
	// set PORTA to 0x00 as soon as possible
	// to hide unwanted blinking after S2 is pressed
	asm volatile(
		"push r16\n\t"
		// PORTA = 0x00;
		"ldi r16, 0x00\n\t"
		"out 0x1b, r16\n\t"
		//_keys_wakeup_requested = true;
		"ldi r16, 1\n\t"
		"sts %0, r16\n\t"
		"pop r16\n\t"
		"reti\n\t"
		: "=m" (_keys_wakeup_requested)
	);
}

ISR(PCINT1_vect, ISR_ALIASOF(PCINT0_vect));

void keys_enable_wakeup(void) {
	// assuming that refresher is stopped!

	// key scanning is disabled, so set them to released just in case
	// this also configures both S1 and S2 as inputs
	keys_init();

	// enable PCINTs
	PCMSK0 = _BV(PCINT7);	// S2 = PA7 = PCINT7
	PCMSK1 = _BV(PCINT10);	// S1 = PB2 = PCINT10
	GIFR = _BV(PCIF1) | _BV(PCIF0);
	GIMSK = _BV(PCIE1) | _BV(PCIE0);
}

void keys_disable_wakeup(void) {
	GIMSK = 0x00;
	_keys_wakeup_requested = false;
}
