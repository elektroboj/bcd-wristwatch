#ifndef _KEYS_H_
#define _KEYS_H_

#include <stdbool.h>
#include <stdint.h>


typedef enum {
	KEY_UP = 0,
	KEY_DOWN,
	KEY_HOLD
} keys_state_t;

typedef void (*keys_handler_t)(uint8_t id, keys_state_t state);


extern volatile bool _keys_wakeup_requested;
extern volatile uint8_t _keys_history[2];


void keys_init(void);
void keys_task(void);
uint8_t keys_read(void);
void keys_enable_wakeup(void);
void keys_disable_wakeup(void);
void keys_set_handler(keys_handler_t kh);
void keys_dummy_handler(uint8_t id, keys_state_t state);

static inline bool keys_wakeup_requested(void) {
	return _keys_wakeup_requested;
}

#endif
