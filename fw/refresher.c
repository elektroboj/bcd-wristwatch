#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/cpufunc.h>
#include <avr/io.h>

#include "refresher.h"
#include "keys.h"


extern uint8_t display_leds0, display_leds1;
static uint8_t cached_leds0, cached_leds1;
static uint16_t millis4;


static void debounce(void) {
	DDRA = 0x00;
	PORTA = _BV(PA7);
	_keys_history[0] = (_keys_history[0] << 1) | ((PINB >> PB2) & 1);
	_keys_history[1] = (_keys_history[1] << 1) | ((PINA >> PA7) & 1);
}

ISR(TIM0_COMPA_vect) {
	++millis4;
	if (!(millis4 & 1)) {
		// debounce keys
		debounce();
		// enable outputs
		PORTA = 0x00;
		DDRA = 0xFF;
		// display the first row
		PORTA = cached_leds0;
	} else {
		uint8_t cur_leds1 = cached_leds1;
		// fetch data for the next refresh cycle
		cached_leds0 = display_leds0;
		cached_leds1 = display_leds1;
		// display the second row
		PORTA = cur_leds1;
	}
}

void refresher_start(void) {
	cached_leds0 = 0x00;
	cached_leds1 = 0x00;
	PRR &= ~_BV(PRTIM0);
	millis4 = -1U;
	OCR0A = 128;
	TCNT0 = 0;
	TCCR0A = _BV(WGM01);
	TCCR0B = _BV(CS00);
	TIFR0 |= _BV(OCF0A);
	TIMSK0 |= _BV(OCIE0A);
}

void refresher_stop(void) {
	refresher_pause();
	TCCR0B = 0x00;
	PRR |= _BV(PRTIM0);
}

ticks_t refresher_get_ticks(void) {
	uint16_t millis4_peek;
	uint8_t old_timsk0;
	old_timsk0 = refresher_pause();
	millis4_peek = millis4;
	refresher_resume(old_timsk0);
	return millis4_peek >> 3;
}
