#ifndef _REFRESHER_H_
#define _REFRESHER_H_

#include <stdint.h>

#include <avr/io.h>


#define MS2TICKS(ms)	((ticks_t)((ms + 16) / 32))


typedef uint8_t ticks_t;


void refresher_start(void);
void refresher_stop(void);
ticks_t refresher_get_ticks(void);

static inline uint8_t refresher_pause(void) {
	uint8_t old_timsk0 = TIMSK0;
	TIMSK0 = old_timsk0 & ~_BV(OCIE0A);
	return old_timsk0;
}

static inline void refresher_resume(uint8_t old_timsk0) {
	TIMSK0 = old_timsk0;
}

#endif
